import xlsx from 'xlsx'
import utils from 'loader-utils'
import pickBy from 'lodash/pickBy'

// options:
// - format: 'raw' for unprocessed xlsx json. blank or 'json' for an object
//   like {sheet1: [row1, row2, ...]} where rows come from `xlsx.util.sheet_to_json`.
// - ignored: a regex for rows and columns to exclude from output, based on their names.
//   By default, this is /^_/ - ignore files that start with a _ (like `_python`).
//   Set to `null` (not `undefined`) to exclude nothing.
export default function loader() {
  this.cacheable && this.cacheable()
  const opts = utils.getOptions(this) || {}
  const wb = xlsx.readFile(this.resourcePath)

  if (!opts.format || opts.format === 'json') {
    const ignored = typeof opts.ignored === 'undefined' ? /^_/ : opts.ignored
    function pred(name) {
      return !ignored || !ignored.test(name)
    }
    const json = Object.keys(wb.Sheets)
    // Ignore sheets named like "_name"
    .filter(pred)
    // https://www.npmjs.com/package/xlsx#utility-functions
    // https://www.npmjs.com/package/xlsx#json
    .reduce((data, name) => Object.assign({}, data, {
      [name]:
        xlsx.utils.sheet_to_json(wb.Sheets[name])
        // ignore *columns* named like "_name" too
        .map(row => pickBy(row, (val, key) => pred(key)))
      }), {})
      return JSON.stringify(json)
  }
  else if (opts.format === 'raw') {
    return JSON.stringify(wb)
  }
  else {
    throw new Error('invalid format: '+opts.format)
  }
}
