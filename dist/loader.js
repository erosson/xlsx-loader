'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = loader;

var _xlsx = require('xlsx');

var _xlsx2 = _interopRequireDefault(_xlsx);

var _loaderUtils = require('loader-utils');

var _loaderUtils2 = _interopRequireDefault(_loaderUtils);

var _pickBy = require('lodash/pickBy');

var _pickBy2 = _interopRequireDefault(_pickBy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// options:
// - format: 'raw' for unprocessed xlsx json. blank or 'json' for an object
//   like {sheet1: [row1, row2, ...]} where rows come from `xlsx.util.sheet_to_json`.
// - ignored: a regex for rows and columns to exclude from output, based on their names.
//   By default, this is /^_/ - ignore files that start with a _ (like `_python`).
//   Set to `null` (not `undefined`) to exclude nothing.
function loader() {
  this.cacheable && this.cacheable();
  var opts = _loaderUtils2.default.getOptions(this) || {};
  var wb = _xlsx2.default.readFile(this.resourcePath);

  if (!opts.format || opts.format === 'json') {
    var pred = function pred(name) {
      return !ignored || !ignored.test(name);
    };

    var ignored = typeof opts.ignored === 'undefined' ? /^_/ : opts.ignored;

    var json = Object.keys(wb.Sheets)
    // Ignore sheets named like "_name"
    .filter(pred)
    // https://www.npmjs.com/package/xlsx#utility-functions
    // https://www.npmjs.com/package/xlsx#json
    .reduce(function (data, name) {
      return Object.assign({}, data, _defineProperty({}, name, _xlsx2.default.utils.sheet_to_json(wb.Sheets[name])
      // ignore *columns* named like "_name" too
      .map(function (row) {
        return (0, _pickBy2.default)(row, function (val, key) {
          return pred(key);
        });
      })));
    }, {});
    return JSON.stringify(json);
  } else if (opts.format === 'raw') {
    return JSON.stringify(wb);
  } else {
    throw new Error('invalid format: ' + opts.format);
  }
}
